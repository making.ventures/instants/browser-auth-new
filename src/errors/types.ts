export enum EErrorTypes {
  UserLocked = 'userLocked',
  EmailNotConfirmed = 'emailNotConfirmed',
  LoginRequired = 'loginRequired',
  InvalidGrant = 'invalidGrant',
}
