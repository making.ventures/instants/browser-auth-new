import {EErrorTypes} from './types';

export class InvalidGrant extends Error {
  public type = EErrorTypes.InvalidGrant;

  public data?: Record<string, any> = {};

  constructor(data?: Record<string, any>) {
    super('Invalid grant');

    this.data = data;

    // Set the prototype explicitly.
    Object.setPrototypeOf(this, InvalidGrant.prototype);
  }
}
