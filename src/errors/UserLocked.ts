import {EErrorTypes} from './types';

export class UserLocked extends Error {
  public type = EErrorTypes.UserLocked;

  public data?: Record<string, any> = {};

  constructor(data?: Record<string, any>) {
    super('User Locked');

    this.data = data;

    // Set the prototype explicitly.
    Object.setPrototypeOf(this, UserLocked.prototype);
  }
}
