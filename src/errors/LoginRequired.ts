import {EErrorTypes} from './types';

export class LoginRequired extends Error {
  public type = EErrorTypes.LoginRequired;

  public data?: Record<string, any> = {};

  constructor(data?: Record<string, any>) {
    super('Login required');

    this.data = data;

    // Set the prototype explicitly.
    Object.setPrototypeOf(this, LoginRequired.prototype);
  }
}
