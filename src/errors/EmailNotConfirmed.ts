import {EErrorTypes} from './types';

export class EmailNotConfirmed extends Error {
  public type = EErrorTypes.EmailNotConfirmed;

  public data?: Record<string, any> = {};

  constructor(data?: Record<string, any>) {
    super('Email Not Verified');

    this.data = data;

    // Set the prototype explicitly.
    Object.setPrototypeOf(this, EmailNotConfirmed.prototype);
  }
}
