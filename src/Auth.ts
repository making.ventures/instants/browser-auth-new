/* eslint-disable @typescript-eslint/camelcase */
import ClientOAuth2, {Token} from 'client-oauth2';
import log from 'loglevel';
import {checkAuthError} from './utils';
import {jstr} from '@instants/core';
import jwtDecode from 'jwt-decode';
import {fromPairs, toPairs} from 'lodash';
import Case from 'case';
import {LoginRequired} from './errors/LoginRequired';
import {InvalidGrant} from './errors';

export interface IAuthSession {
  accessToken: string;
  idToken?: string;
  refreshToken?: string;
  authUserData?: IAuthUserData;
  appState?: any;
}

export interface IAuthUserData {
  email?: string;
  name?: string;
  nickname?: string;
  picture?: string;
  emailVerified?: string;
}

export interface IAuthConf {
  accessTokenUri: string;
  authUri: string;
  authorizationUri: string;
  registerUri: string;
  clientId: string;
  tenantId: string;
  logoutUri: string;
  query: {
    audience: string;
  };
  redirectUri: string;
  scopes: string[];
  authSessionKey: string;
  mode: 'code' | 'token';
}

export class Auth {
  private readonly conf: IAuthConf;

  private readonly auth: ClientOAuth2;

  constructor(conf: IAuthConf) {
    conf.mode = conf.mode || 'code';
    this.conf = conf;
    this.auth = new ClientOAuth2(conf);
  }

  private loginTokenUri(silent = false) {
    const authUri = this.auth.token.getUri({
      query: {
        ...this.conf.query,
        ...{response_type: 'id_token token'},
        ...(silent ? {prompt: 'none'} : {}),
      },
    });

    return authUri;
  }

  private loginCodeUri(silent = false) {
    const authUri = this.auth.code.getUri({
      query: {
        ...this.conf.query,
        ...(silent ? {prompt: 'none'} : {}),
      },
    });

    return authUri;
  }

  private loginTokenFlow(silent = false) {
    const authUri = this.loginTokenUri(silent);
    log.info(`loginTokenFlow, authUri: ${authUri}`);

    window.location.replace(authUri);
  }

  private loginCodeFlow(silent = false) {
    const authUri = this.loginCodeUri(silent);
    log.info(`login, authUri: ${authUri}`);

    window.location.replace(authUri);
  }

  public async login(silent = false) {
    const session = this.getAuthSession();
    if (session && session.refreshToken) {
      const token = this.auth.createToken(
        session.accessToken,
        session.refreshToken,
        {},
      );

      const refreshedToken = await new Promise<Token>((resolve, reject) => {
        try {
          token.refresh()
            .then((token) => resolve(token))
            .catch(error => {
              reject(error);
            });
        } catch (error) {
          log.error(`error: ${error}, obj: ${jstr(error)}`);
        }
      });

      // log.info(
      //   `!!!!!!!!!!!!!!!!!! login flow keys: ${Object.keys(
      //     refreshedToken,
      //   )}, client: ${refreshedToken.client}, data: ${jstr(
      //     refreshedToken.data,
      //   )}, tokenType: ${refreshedToken.tokenType}, accessToken: ${
      //     refreshedToken.accessToken
      //   }, refreshToken: ${refreshedToken.refreshToken}`,
      // );

      const refreshedSession = this.tokenToSession(refreshedToken);

      this.setAuthSession(refreshedSession);

      return refreshedSession;
    }

    if (this.conf.mode === 'code') {
      return this.loginCodeFlow(silent);
    } else {
      return this.loginTokenFlow(silent);
    }
  }

  private async checkIsCallbackPage() {
    return location.href.startsWith(this.conf.redirectUri);
  }

  public async getTokenFromUri(): Promise<IAuthSession | null> {
    log.info(
      `takeTokenFromUri. window.location.href: ${
        window.location.href
      }, window.location.href.includes('access_token'): ${window.location.href.includes(
        'access_token',
      )}`,
    );

    const isLoginPage = this.checkIsCallbackPage();

    if (!isLoginPage) {
      return null;
    }

    // Code flow
    if (window.location.href.includes('code')) {
      log.info('taking code from uri');

      return new Promise<IAuthSession>((resolve, reject) =>
        this.auth.code
          .getToken(window.location.href, {})
          .then((token: Token) => {
            log.info(
              '%crenewSessionFusion, Session updated!',
              'font-weight: bold',
            );

            log.info(
              `getTokenFromUri Code flow keys: ${Object.keys(token)}, client: ${
                token.client
              }, data: ${jstr(token.data)}, tokenType: ${
                token.tokenType
              }, accessToken: ${token.accessToken}, refreshToken: ${
                token.refreshToken
              }`,
            );

            const session = this.tokenToSession(token);
            this.setAuthSession(session);
            resolve(session);
          })
          .catch((error) => {
            log.error(
              `renewSessionFusion, HttpClient. Error while renewing session, error obj: ${jstr(
                error,
              )}, error: ${error}`,
            );

            this.removeAuthSession();
            reject(error);
            throw checkAuthError(error);
          }),
      );
    }

    // Token flow
    if (window.location.href.includes('access_token')) {
      log.info('taking token from uri');

      return new Promise<IAuthSession>((resolve, reject) =>
        this.auth.token
          .getToken(window.location.href)
          .then((result: Token) => {
            log.info(
              '%crenewSessionFusion, Session updated!',
              'font-weight: bold',
            );

            log.info(
              `renewSessionFusion keys: ${Object.keys(result)}, client: ${
                result.client
              }, data: ${jstr(result.data)}, tokenType: ${
                result.tokenType
              }, accessToken: ${result.accessToken}, refreshToken: ${
                result.refreshToken
              }`,
            );

            const session = {
              accessToken: ((result as unknown) as IAuthSession).accessToken,
              idToken: ((result as unknown) as IAuthSession).idToken,
            };

            resolve(session);
          })
          .catch((error) => {
            log.error(
              `renewSessionFusion, HttpClient. Error while renewing session, error obj: ${jstr(
                error,
              )}, error: ${error}`,
            );

            reject(error);
            throw checkAuthError(error);
          }),
      );
    }

    // Error
    if (window.location.href.includes('error')) {
      log.info('error handling');
      log.info(`error handling, window.location.href: ${window.location.href}`);
      const searchParams = new URLSearchParams(window.location.search);
      const error = searchParams.get('error');
      log.info(`error handling, error: ${searchParams.get('error')}`);
      log.info(
        `error handling, error_description: ${searchParams.get(
          'error_description',
        )}`,
      );

      if (error === 'login_required') {
        throw new LoginRequired();
      } else if (error === 'invalid_grant') {
        throw new InvalidGrant();
      } else {
        throw new Error(
          `Error ocerred, error: ${error}, description: ${searchParams.get(
            'error_description',
          )}`,
        );
      }
    }

    return null;
  }

  public authenticated() {
    return this.hasAuthSession();
  }

  public logout() {
    log.info('logout');
    if (this.hasAuthSession()) {
      this.removeAuthSession();
    }

    const redirect = encodeURIComponent(this.conf.redirectUri);

    const uri = `${this.conf.logoutUri}?client_id=${this.conf.clientId}&post_logout_redirect_uri=${redirect}&returnTo=${redirect}`;
    log.info(`logout uri: ${uri}`);
    window.location.replace(uri);
  }

  public getAuthUserData() {
    const session = this.getAuthSession();

    return session ? session.authUserData || null : null;
  }

  private setAuthSession(authSession: IAuthSession) {
    if (!authSession) {
      throw new Error('authSession should be passed');
    }
    localStorage.setItem(this.conf.authSessionKey, jstr(authSession));
  }

  private tokenToSession(token: Token) {
    const session = {
      accessToken: ((token as unknown) as IAuthSession).accessToken,
      authUserData: token.data.id_token ?
        fromPairs(
          toPairs(
            jwtDecode<IAuthUserData>(token.data.id_token),
          ).map(([key, value]) => [Case.camel(key), value]),
        ) :
        undefined,
      idToken: token.data.id_token,
      refreshToken: ((token as unknown) as IAuthSession).refreshToken,
    };

    return session;
  }

  public getAuthSession() {
    if (typeof window === 'undefined') {
      return null;
    }
    const session = localStorage.getItem(this.conf.authSessionKey);

    if (session) {
      return JSON.parse(session) as IAuthSession;
    } else {
      return null;
    }
  }

  private hasAuthSession() {
    return Boolean(this.getAuthSession());
  }

  private removeAuthSession() {
    return localStorage.removeItem(this.conf.authSessionKey);
  }
}
