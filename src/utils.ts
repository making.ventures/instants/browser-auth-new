import {Auth0Error} from 'auth0-js';
import log from 'loglevel';
import {EmailNotConfirmed, LoginRequired, InvalidGrant} from './errors';
import {jstr} from '@instants/core';

export const checkAuthError = (error: Auth0Error | any): Error => {
  log.warn(`Checking error. Error: ${error}, error: ${jstr(error)}`);
  if (
    error.code &&
    error.code === 'unauthorized' &&
    error.description &&
    error.description === 'Please verify your email before logging in.'
  ) {
    return new EmailNotConfirmed();
  } else if (error.code && error.code === 'login_required') {
    return new LoginRequired();
  } else if (error.code && error.code === 'invalid_grant' ||
    error.body && error.body.error && error.body.error === 'invalid_grant') {
    return new InvalidGrant();
  }

  return error as Error;
};
